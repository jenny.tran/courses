
<div class=leader>
<i class="twa twa-mechanical-arm"></i>
<i class="twa twa-battery"></i>
<i class="twa twa-nut-and-bolt"></i>
<i class="twa twa-mechanical-leg"></i>
<br>
Compiler &amp; language internals
(part 1)
</div>



# How do I package my code?

Use `] generate MyPackageName`.
- `src/MyPackageName.jl` is the main file of the package
  - use `include(...)` to add more files
- if you want things exported (auto-imported with `using`), mark them with `export`

```julia
module MyPackage
f() = 123
export f
end
```

- `Project.toml` contains metadata and dependencies
  - use `] activate` to add/remove dependencies easily
- `test/runtests.jl` is the main entrypoint of unit tests

```
using Test, MyPackageName
@testset "My test set" begin
  @test f() == 123
end
```



# How do I document my code?

Docstrings are automatically sourced and exposed to the help system (and `?`)

```julia
"""
    f()

This returns a pretty good randomly chosen constant.
"""
f() = 123
```

Extra packages:
- `Documenter.jl` makes a nice website out of the documentation
  - (and uploads it to e.g. github pages)
- `DocStringExtensions.jl` provide some macros to simplify writing docstrings
  - (e.g., typesetting of the full function type signature)
