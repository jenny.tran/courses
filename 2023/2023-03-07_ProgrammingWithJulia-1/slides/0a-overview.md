# <i class="twa twa-information"></i> Course preliminaries

- Everything is in the slides
- The slides (current version) are available at `courses.lcsb.uni.lu`
- Ask questions immediately
- Offline questions best sent by e-mail
  - `miroslav.kratochvil@uni.lu`
  - `laurent.heirendt@uni.lu`



# <i class="twa twa-red-question-mark"></i> What do you need to do?

- Participate in the course
  - We'll try to include as much practicals & fun as possible
  - Feel free to bring a laptop and code along
- Do the homework
  - 3 easy tasks (more about thinking than programming)
- Come to the exam
  - We'll check if you did the homework

*Exceptions are possible*, but let us know ASAP
- do not wait to the last possible moment until it's absolutely clear that a problem will happen



# <i class="twa twa-ledger"></i> Homework

- *HW 0*: trivial, touch the tooling
  - Solution period: 7.3. — *14.3*.
- *HW 1*: basic programming
  - Solution period: 7.3. — *21.3*.
- *HW 2*: making your code go fast
  - Solution period: 14.3. — *28.3*.
- *HW 3*: managing types and complex things
  - Solution period: 21.3. — *exam*

## Guidelines

- Homework will appear in Moodle. Submit solutions via Moodle.
- If you cooperate, declare how you made sure it's fair.
- Simple and short solutions are better than tons of code.



# Course overview

1. *(7.3.)* Start-up and basics
2. *(9.3.)* In-depth language features (arrays, types, compilation, speed, macros)
3. *(14.3.)* Scientific computing: Basics
4. *(16.3.)* Scientific computing: Useful packages
5. *(21.3.)* Optimization, parallelization and acceleration
6. *(23.3.)* Types, subtypes and complex data

- Exam on 30.3., same time



# <i class="twa twa-woman-teacher"></i> Knowing the demographics is important!

- Our background is almost entirely technical.
  - We did several scientific computing packages for Julia
  - MK was previously teaching a pretty wide spectrum of other programming languages
- What do *you* currently use for programming?
- What's *your* area of interest?
