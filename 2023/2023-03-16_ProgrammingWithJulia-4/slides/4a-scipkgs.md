<div class=leader>
<i class="twa twa-horse-face"></i>
<i class="twa twa-zebra"></i>
<i class="twa twa-chicken"></i>
<i class="twa twa-unicorn"></i>
<br>
Ecosystem
</div>



# Cornerstones

- `Symbolics.jl`
- `ForwardDiff.jl`
- SciMl
- `JuMP.jl`
- `Flux.jl`



# Symbolics.jl

- declare symbolic variables: `@variables`
  - tricky question: can this be done without a macro?
- `substitute`
- `derivative`
- `solve_for` (linear eqns)
- `build_function`
- `using Latexify` → `latexify`



# SYSTEMATIC INTERLUDE

What is the difference between:
- a variable `a`
- a variable `a` that was marked as a `@variable`
- a symbol `:a`
...?

(How does that extend to expressions?)



# ForwardDiff.jl

Main task:
- generate better functions than Symbolics
- avoid the intermediate symbolic phase
- support arrays

```julia
f(x) = 4*x[1]^2 - 3*x[2]^3 + 2*x[3]^4 + 32 - 5*x[4] + 2*x[1]*x[2]*x[3] + 2*x[2] + x[2]*x[3]*x[4] + 2*x[1]*x[3]+x[4] + 3*(x' * x)
```

Task: find the minimum of the function (with gradient descent)

Pro-level: try with Symbolics



# SciML.jl

If you are into:
- ODEs
- ODE solvers
- Reaction networks
- dynamic systems
- N-body simulations
- nonlinear optimization
...SciML probably has a package for it.



# JuMP.jl

JuMP is for constrained optimization (LP, MILP, QP).

- make a `Model` using optimizers (let's use `GLPK.Optimizer`)
- `@variables`
- `@constraint`
- `@objective`
- `optimize!`
- `value`

Task:
- Factory A produces 5 drinks and 1 foods for 1 coal
- Factory B produces 1 drink and 4 foods for 1 coal
- People need roughly 10 drinks for 7 foods
- Each person needs 1 food a day, and you have 100 persons
- How much coal do you need daily?



# Flux.jl

Flux is like the Keras stack, but written in a single language.

Let's have a look at the website.
