
<div class=leader>
<i class="twa twa-blueberries"></i>
<i class="twa twa-red-apple"></i>
<i class="twa twa-melon"></i>
<i class="twa twa-grapes"></i><br>
Questions?
</div>



# Thank you!

<center><img src="slides/img/r3-training-logo.png" height="200px"></center>

Contact us if you need help:

<a href="mailto:lcsb-r3@uni.lu">lcsb-r3@uni.lu</a>

