# Motivation first!

*Why is it good to work in compiled language?*

- Programs become much faster for free.
- Even if you use the language as a package glue, at least the glue is not slow.

*What do we gain by having types in the language?*

- Generic programming, and lots of optimization possibilities for the compiler.

*Is Julia ecosystem ready for my needs? <i class="twa twa-thinking-face"></i>*

- Likely. If not, extending the packages is super easy.
- Base includes most of the functionality of Matlab, R and Python with numpy,
  and many useful bits of C++



# Why Julia?

<center><img src="slides/img/whyjulia.png" width="80%"></center>

(Source: JuliaCon 2016, Arch D. Robison)
