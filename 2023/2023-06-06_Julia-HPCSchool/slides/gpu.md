
<div class=leader>
<i class="twa twa-volcano"></i>
<i class="twa twa-mount-fuji"></i>
<i class="twa twa-snow-capped-mountain"></i>
<i class="twa twa-mountain"></i>
<i class="twa twa-sunrise-over-mountains"></i>
<br>
Utilizing GPUs
</div>



# Note about CUDA

Julia can serve as an extremely user-friendly front-end for CUDA, abstracting all ugly steps that you'd need to do with normal CUDA, yet still leaving enough flexibility to write high-performance low-level compute kernels.

The approach here demonstrates what `CUDA.jl` does.

There's also:

- `AMDGPU.jl`
- `Metal.jl` for <i class="twa twa-green-apple"></i>
- `Vulkan.jl` (less user friendly but works everywhere)



# Using your GPU for accelerating simple stuff

```julia
julia> data = randn(10000,10000);

julia> @time data*data;

julia> using CUDA

julia> data = cu(data);

julia> @time data*data;
```



# What's available?

The "high-level" API spans most of the CU* helper tools:

- broadcasting numerical operations via translation to simple kernels (`.+`, `.*`, `.+=`, `ifelse.`, `sin.`, ...)
- matrix and vector operations using `CUBLAS`
- `CUSOLVER` (solvers, decompositions etc.) via `LinearAlgebra.jl`
- ML ops (in `Flux.jl`): `CUTENSOR`
- `CUFFT`
- `CUSPARSE` via `SparseArrays.jl`
- limited support for reducing operations (`findall`, `findfirst`, `findmin`, ...) -- these do not translate easily to GPU code
- very limited support for array index processing

(See: https://github.com/NVIDIA/CUDALibrarySamples)



# Programming kernels in Julia!

CUDA kernels (`__device__` functions) are generated transparently directly from Julia code.

```julia
a = cu(someArray)

function myKernel(a)
    i = threadIdx().x
    a[i] += 1
    return
end

@cuda threads=length(a) myKernel(a)
```

Some Julia constructions will not be feasible on the GPU (mainly allocating complex structures); these will trigger a compiler message from `@cuda`.



# Programming kernels -- usual tricks

The amount of threads and blocks is limited by hardware; let's make a
grid-stride loop to process a lot of data quickly!

```julia
a = cu(someArray)
b = cu(otherArray)

function applySomeMath(a, b)
    index = threadIdx().x + blockDim().x * (blockIdx().x-1)
    gridStride = gridDim().x * blockDim().x
    for i = index:gridStride:length(a)
        a[i] += someMathFunction(b[i])
    end
    return
end

@cuda threads=1024 blocks=32 applySomeMath(a)
```

Typical CUDA trade-offs:
- too many blocks won't work, insufficient blocks won't cover your SMs
- too many threads per block will fail or spill to memory (slow), insufficient threads won't allow parallelization/latency hiding in SM
- thread divergence destroys performance



# CUDA.jl interface

Functions available in the kernel:

- `gridDim`, `blockDim`
- `blockIdx`, `threadIdx`
- `warpsize`, `laneid`, `active_mask`
- `sync_threads`, `sync_warp`, `threadfence`, ...
- `vote_all`, `vote_ballot`, `shfl_sync`, ...

Parameters for the `@cuda` spawn:
- `threads=nnn` per block
- `blocks=nnn` per grid
- `shmem=nnn` how much shared memory to request (available via `CuStaticSharedArray`)
