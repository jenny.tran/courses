
# Julia for High Performance Scientific Computing

## June 6th, 2023

<div style="top: 6em; left: 0%; position: absolute;">
    <img src="theme/img/lcsb_bg.png">
</div>

<div style="top: 1em; left: 60%; position: absolute;">
    <img src="slides/img/r3-training-logo.png" height="200px">
    <img src="slides/img/julia.svg" height="200px">
    <h1 style="margin-top:3ex; margin-bottom:3ex;">Julia for HPCs</h1>
    <h4>
        Miroslav Kratochvíl, Ph.D.<br>
        Laurent Heirendt, Ph.D.<br>
        R3 Team - <a href="mailto:lcsb-r3@uni.lu">lcsb-r3@uni.lu</a><br>
        <i>Luxembourg Centre for Systems Biomedicine</i>
    </h4>
</div>

<link rel="stylesheet" href="https://lcsb-biocore.github.io/icons-mirror/twemoji-amazing.css">
<style>
	code {border: 2pt dotted #f80; padding: .4ex; border-radius: .7ex; color:#444; }
	.reveal pre code {border: 0; font-size: 18pt; line-height:27pt;}
	em {color: #e02;}
	li {margin-bottom: 1ex;}
	div.leader {font-size:400%; line-height:120%; font-weight:bold; margin: 1em;}
	section {padding-bottom: 10em;}
</style>
