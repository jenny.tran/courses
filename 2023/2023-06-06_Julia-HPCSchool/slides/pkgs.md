
<div class=leader>
<i class="twa twa-bar-chart"></i>
<i class="twa twa-blue-book"></i>
<i class="twa twa-computer-disk"></i>
<i class="twa twa-chart-increasing"></i><br>
Packages for <br>doing useful things
</div>



# How do I do ... ?

- Structuring the data: `DelimitedFiles`, `CSV`, `DataFrames`
- Working with large data: `DistributedArrays`, `LabelledArrays`
- Stats: `Distributions`, `StatsBase`, `Statistics`
- Math: `ForwardDiff`, `Symbolics`
- Problem solving: `JuMP`, `DifferentialEquations`
- ML: `Flux`
- Bioinformatics: `BioSequences`, `GenomeGraphs`
- Plotting: `Makie`, `UnicodePlots`
- Writing notebooks: `Literate`



# Data frames

Package `DataFrames.jl` provides a work-alike of the data frames from
other environments (pandas, `data.frame`, tibbles, ...)

```julia
using DataFrames

mydata = DataFrame(id = [32,10,5], text = ["foo", "bar", "baz"])

mydata.text

mydata.text[mydata.id .>= 10]
```

Main change from `Matrix`: *columns are labeled and their types differ*, also entries may be missing



# DataFrames

Popular way of importing data:
```julia
using CSV
df = CSV.read("database.csv", DataFrame)       # can also do a Matrix

CSV.write("backup.csv", df)
```

Popular among computer users:
```julia
using XLSX
x = XLSX.readxlsx("important_results.xls")

XLSX.sheetnames(x)

DataFrame(XLSX.gettable(x["Results sheet"])...)
```

<small>(Please do not export data to XLSX.)</small>



# Plotting

<center>
<img src="slides/img/unicodeplot.png" width="40%" />
</center>
