
<div class=leader>
<i class="twa twa-rocket"></i>
<i class="twa twa-rocket"></i>
<i class="twa twa-rocket"></i>
<i class="twa twa-rocket"></i>
<br>
Parallel Julia
</div>



# Usual ways to gain performance:

1. Do not work on data that is too far *(cache)*
2. Do not waste energy on organizing trivial stuff *(SIMD/SIMT)*
3. Do not waste time waiting for data *(HT/GPU)*
4. Organize the computation so that more computers don't trip over each other on the task *(SMP)*
5. Move the computers closer to data *(distributed computing)*

Let's spend a moment explaining these technologies...



# Data distance and physical limits

<div class=leader>
How far does light go in 1 cycle of a 3GHz CPU?
</div>



# CPUs vs GPUs (SIMD vs SIMT)

<center>
<img src="slides/img/cpu.png" width="40%" />
<img src="slides/img/gpu.png" width="40%" />
</center>



# SIMD problem: Your Data Looks Like This™

<center>
<img src="slides/img/maze.jpg" width="75%" />
</center>



# Parallel programming

<center>
<img src="slides/img/threads.jpeg" width="50%" /><br>
(notice the false sharing at threaddog 5)
</center>

Distributed computing can give you:

- more memory
- more total memory bandwidth (!)
- more synchronization problems



# Julia tools

Let's implement a matrix multiplication manually, and try:

- checking if SIMD instructions are used
- reordering the loops
- using smaller floats
- tiling
- `@threads`

<center><img src="slides/img/tiling.jpeg" width="33%" /></center>



# Distributed computing with Julia

```julia
using Distributed
addprocs(10)

pmap(myfunction, mydata, workers=workers())
```

(Spoiler: you can add processes on remote machines using SSH.)



# Distributed computing with Julia (on the HPC)

```julia
using Distributed, ClusterManagers
addprocs_slurm(parse(Int, ENV["SLURM_NTASKS"]))

pmap(myfunction, mydata, workers=workers())
```

You typically want to load the data locally at the workers.

For more complex schemes:
- `Dagger.jl` provides complex synchronization/task dependency schemes
- `DistributedData.jl` provides primitives for manipulating the data precisely



# How to use a GPU?

```julia
using CUDA

A = cu(randn(1000,1000));
B = cu(randn(1000,1000));
A = A * B;
```

...transparently uses CUDA, cuBLAS, cuSPARSE, cuDNN and many other libraries to do stuff quicker.



# How to actually program a GPU?

CUDA.jl can compile Julia code into CUDA kernel code.

```julia
function fill_with_indexes!(array)
    index = threadIdx().x + blockDim().x * (blockIdx().x - 1)
    stride = gridDim().x * blockDim().x
    for i = index:stride:length(arr)
        arr[i] = i
    end
    return
end

A = cu(zeros(9999999))
@cuda threads=1024 blocks=16 fill_with_indexes!(A)
```



# Homeworks

## Homework 2 update

Feel free to apply whatever we did today for bonus points.

- think about cache efficiency
- remember that most forces are repulsive forces
- `@threads` may help for larger graphs

## Homework 3

- We will learn to handle ugly and hairy data.
- Last lecture is going to go over the methodology, but you can try earlier.



# Homework 3

We will simulate a cookie distribution network:
- there's one central *cookie factory*
- cookie *transports* move cookies in loads of N cookies
  - they require 1 cookie to sustain themselves for each batch of cookies transported
  - N is different for each transport
- cookie *distribution points* divide incoming cookies among next paths in networks
  - exact distribution ratio among the paths
  - no cookies consumed
- cookie *munchers* are at the end of the transport chain
  - each of them munches N cookies per day (again different for each muncher)



# Homework 3 (Assignment)

Tasks:
- read the cookie network from a JSON file (we'll provide example data, use `JSON.jl`)
- make a nice data structure to hold this problem, make sure the input is valid
- make functions that:
  - find the length of the *longest chain* (by transport "steps") from the factory to the muncher
  - find out how many cookies the factory needs to produce daily so that *all munchers are fed*
  - construct a network where all *transports are split in half*, each half with half cookie consumption
  - find out *how many cookies are wasted* by being routed to munchers who can't eat them
  - construct a network where the *distribution points are balanced* so that no cookies get wasted 
  - *BONUS: print the network nicely*
- for simplicity, data structures and functions may be recursive
  - performance optimization _is not_ a goal
  - nice short code _is_ a goal




# Homework 3 (Example data)

```json
{ "type": "distribution point",
  "serves": [
    { "type": "muncher",
      "consumption": 3 },
    { "type": "transport",
      "capacity": 5,
      "serves": {
        "type": "distribution point",
        "serves": [
          { "type": "muncher",
            "consumption": 7 },
          { "type": "muncher",
            "consumption": 2 },
          { "type": "muncher",
            "consumption": 1 } ],
        "ratios": [ 1, 1, 1 ]
      }
    }
  ],
  "ratios": [ 1, 5 ]
}
```




# Homework 3 (expected results on the example data)

- make one common abstract type for "everything in the network"
- longest chain length: 3 (any of the "nested" munchers is at the longest chain)
- required daily production for feeding all munchers: 36 (7+7+7 cookies for the nested munchers because of 1:1:1 ratio, that needs 5 cookies for transport (26 total), and overfeeds the first muncher by 3 cookies because of 6+30 in ratio 1:5). Alternative ways to compute are OK (if allowing non-integer cookies, we'd get something like 30.24 daily consumption).
- transports split in half: instead of `... -> transport 5 -> ...` the network should contain something like `... -> transport 10 -> transport 10 -> ...`.
- how many cookies are wasted: the easiest way to guess is to assume perfect distribution and make a difference from the required daily production. In this case, if distribution ratios were perfect, the network would consume 15 cookies daily (10 for the nested munchers + 2 for transport + 3 for the other muncher), which wastes 21 or 15.24 cookies depending on the interpretation.




# Homework 3 (expected results)

Pretty-printed example network (one possibility):
```
1 -> munch 3
5 -> transport 5 -> 1 -> munch 7
                    1 -> munch 2
                    1 -> munch 1
```

Balanced distribution network (you don't need to pretty-print the modified network! here we are just showing it in the pretty format):
```
1 -> munch 3
4 -> transport 5 -> 7 -> munch 7
                    2 -> munch 2
                    1 -> munch 1
```

This is equally valid:
```
3  -> munch 3
12 -> transport 5 -> 7 -> munch 7
                     2 -> munch 2
                     1 -> munch 1
```

