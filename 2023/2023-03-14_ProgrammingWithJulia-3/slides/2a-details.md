
<div class=leader>
<i class="twa twa-mechanical-arm"></i>
<i class="twa twa-battery"></i>
<i class="twa twa-nut-and-bolt"></i>
<i class="twa twa-mechanical-leg"></i>
<br>
Compiler &amp; language internals (continued)
</div>



# What is this "compiled code"?

You can inspect various stages of code representation (and reveal bugs):

- `code_lowered(+)`
- `code_typed(+)`
- `code_typed(+, (Int, Float64))`
- `code_llvm`
- `code_native`

What happens if we use an abstract type?



<center style="font-size:200%; line-height:120%; margin-bottom: 1em">
Performance rule #1:<br><br>
<strong>Less instructions is typically better,</strong><br>
<strong>less jumps is typically much better.</strong>
</center>



<center style="font-size:200%; line-height:120%; margin-bottom: 1em">
Performance rule #2:<br><br>
<strong>Memory allocations comprise of many instructions<br> and plenty of jumps.</strong>
</center>



# What are all these things starting with `@`?

Macros allow you to generate code before compiler runs it:
- `@info` can know what variable names to display
- `@benchmark` can run the code several times
- `@test` can safely wrap the failing code and print what failed

## Important types for macros:

- symbols: `:abc`, `:+`, ...
- expressions: `:(1+2)`, `quote ... end`

Macros are functions that get unevaluated arguments (as expressions) and return unevaluated results (as expressions).

```julia
macro clear_data(v)
  quote
    $(Symbol(v, :_orig)) = load_original_data()
    $(Symbol(v, :_enriched)) = nothing
    $(Symbol(v, :_final)) = nothing
  end
end

@clear_data patients   # reloads patients_orig, clears patients_enriched, patients_final
```



# Macro use-cases

Use macros to improve your code whenever Julia syntax is too unwieldy.

```julia
model = @reaction_network MyModel begin
    c1, s + i --> 2i
    c2, i --> r
end
```

(Taken from `Catalyst.jl`)
