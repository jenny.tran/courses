
<div class=leader>
<i class="twa twa-volcano"></i>
<i class="twa twa-mount-fuji"></i>
<i class="twa twa-snow-capped-mountain"></i>
<i class="twa twa-mountain"></i>
<i class="twa twa-sunrise-over-mountains"></i>
<br>
Julia for scientific computing 
</div>



# What you mostly do in scientific/numeric computing?

- `LinearAlgebra.jl`, `SparseArrays.jl`
  - easily utilizes `CUDA.jl`, `AMDGPU.jl`, `Metal.jl`
- statistics
- simulations (typically custom made)
- neural networks, ODEs, constrained problems (next time)
- plotting!



# Statistics in Julia

- `StatsBase.jl` for all "base" runctions (correlations, medians, quantiles, ranks, ...)
- `Distances.jl` for measuring distances/similarities
- `Distributions.jl` for generating numbers
- `HypothesisTests.jl` for getting p-values
- `Clustering.jl`

See more at [juliastats.org](https://juliastats.org)



# Mini-exercise

- Plot the correlations between the columns of our maze in a heatmap
- Order the heatmap by similarity (from hclust) to visualize clusters



# Simulation exercise: Let's layout some graphs!

<img src="slides/img/graph.jpeg" width="70%">



# Simulation exercise: Let's layout some graphs!

- Read a set of edges (integer pairs)
- Simulate that the vertices are pushed around by 2 forces:
  - quadratic attractive force ("gravity")
  - inverse quadratic attractive force
  - no momentum or any other kind of physics!
- Draw what happens after a few rounds of simulation



# Exercise: Let's align some <i class="twa twa-dna"></i>DNA<i class="twa twa-dna"></i>!

```
  - S a t u r d a y
- 0 1 2 3 4 5 6 7 8
S 1 0 1 2 3 4 5 6 7
u 2 1 1 2 2 3 4 5 6
n 3 2 2 2 3 3 4 5 6
d 4 3 3 3 3 4 3 4 5
a 5 4 3 4 4 4 4 3 4
y 6 5 4 4 5 5 5 4 3

* not actual DNA
```



# Exercise: Let's align some <i class="twa twa-dna"></i>DNA<i class="twa twa-dna"></i>!

Let's do bioinformatics; simple short 2-sequence alignment can be done with
Levenshtein algorithm.

- Print out the alignment nicely (is it unique?)
```
.++.*...
Saturday
S  unday
.--.*...
```
- Make the "costs" easily parametrizable
  - some DNA mutations are more probable than others
  - what's a good way to parametrize the functionality?
