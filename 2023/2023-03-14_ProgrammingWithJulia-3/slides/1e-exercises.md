
# <i class="twa twa-pencil"></i> Exercise (continued): median

Let's try to shrink the approximate median function code by 90%.



# <i class="twa twa-pencil"></i> Exercise: let's play the maze game (again)

- Make a function that annotates the whole maze based on how many (axis-aligned) steps a person (maze inhabitant) needs to take from some point
  - let's use the slow <i class="twa twa-water-wave"></i>wave<i class="twa twa-water-wave"></i> algorithm
- Use `UnicodePlots` to plot interesting stuff
  - shortest path length distribution
  - shortest-path-length heatmap of the maze
  - "most blocking walls" (how much time to reach the father side of the wall could be gained by removing the wall?)



# Exercise: Tricky questions about the maze

What is the slowest part of the "wave" solution?

How can we make it faster?
