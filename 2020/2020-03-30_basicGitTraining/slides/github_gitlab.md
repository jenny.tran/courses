# GitHub and GitLab

<img src="https://github.githubassets.com/images/modules/logos_page/GitHub-Mark.png" alt="GitHub" style="width: 200px;"/>
<img src="https://gitlab.com/gitlab-com/gitlab-artwork/raw/master/logo/logo-extra-whitespace.png" alt="GitLab" style="width: 200px;"/>

GitHub and GitLab are VCS systems.

GitHub/Gitlab are both **publicly available**, but GitLab can be **on-premise**.

Positive point: GitHub and GitLab are (almost) the same.


<img src="slides/img/icon-live-demo.png" height="100px">


- **GitHub**: [https://github.com](https://github.com)
- Public GitLab: [https://gitlab.com](https://gitlab.com)
- LCSB specific: [https://git-r3lab.uni.lu](https://git-r3lab.uni.lu)