# What is a `fork`?

<center>
<img src="slides/img/fork.jpg" class="as-is" height="500em"/>
</center>
<!--http://www.cndajin.com/data/wls/246/22302193.jpg-->



# Not really ...

<center>
<img src="slides/img/fork-crossed.png" class="as-is" height="500em"/>
</center>



# What is a `fork`?

- In general, when contributing to a repository, you only have **read** access.
- In other words, you can only **pull** (unless it is your own repository or access has been granted).
- In general, you **cannot write** changes. In other words, you do not have **push** access.
- You have to work on your **own copy**.
- In other words, you have to work on your own <font color="red">**fork**</font>.



# How to get a fork?

Browse to the original repository and click on the button `Fork`:

![Fork the repo](https://help.github.com/assets/images/help/repository/fork_button.jpg)

<img src="slides/img/icon-live-demo.png" height="100px">



# Time to practice!

Fork the practice repository: <br><br>
https://git-r3lab.uni.lu/R3/school/git/basic-practice-pages<br><br>


Then, clone your fork to your home directory!

<img src="slides/img/icon-live-demo.png" height="100px">

```bash
$ git clone ssh://git@git-r3lab-server.uni.lu:8022/<yourName>/basic-practice-pages.git
```

Change to the practice directory with:
```bash
$ cd basic-practice-pages
```



# A note on shortcuts ...

<font color="red">
Any other rudimentary method such as

*'I simply download the `.zip` and unzip it - works like a charm!'*

shall **be avoided**!
</font>

**Why?**



# How to update my fork?

As you have your own fork, it will not automatically be updated once the original repository is updated.

![bulb](slides/img/bulb.png) You have to update it yourself!

<br>

**More on that later!**

