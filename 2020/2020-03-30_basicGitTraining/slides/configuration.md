# How to configure `git`?

```bash
$ git config --global user.name "Firstname Lastname"
$ git config --global user.email "first.last@uni.lu"
```

Test whether your username and email have been registered

```bash
$ git config --list
```

This should list the configuration with `user.name` and `user.email`.

Exit by typing `q`.



# What is an SSH key?

An SSH key is a secure access credential.

**Principle**: <br><br>
Communicate **securely** with Github/Gitlab **without** entering the username/password.



# How do I get and set my SSH key?

Check if you already have an SSH key:

```bash
$ ls -al ~/.ssh
```

If there are 2 files named `id_rsa`, you have an SSH key.

If you don’t have yet an SSH key, you have to generate one:
```bash
$ ssh-keygen -t rsa # -b 4096
```

If you set a password to your key (recommended), add it to the `ssh-agent`:
```bash
$ eval "$(ssh-agent -s)"
$ ssh-add -K ~/.ssh/id_rsa
```

Then, add the SSH key to Github/Gitlab.

<img src="slides/img/icon-live-demo.png" height="100px">