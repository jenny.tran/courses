# Merge (pull) requests

If you want your changes to be reflected on the `develop` or `master` branches,
**submit a merge request (MR)** via the git-r3lab interface.

Use the **interface** to make use of your peers to review your code!
<img src="slides/img/branch-merge.png" class="branch-merge" height="500em"/>

Once merged, you can delete the branch via the interface.

<div class="fragment">

<img src="slides/img/icon-live-demo.png" height="100px" >