# Accessing Computers over Network (Securely)

* Normally, very few ports are open on a modern (LCSB) server

<!-- .element: class="fragment" -->
* In addition, the communication to and from ports is typically encrypted
  - Why? Because, between the sender and a recepient there is a whole
    jungle of transfer network nodes, each of which is potentially
    [p(owned)][1] by an adversary
	
  <!-- .element: class="fragment" -->
  - For a thorough introduction, see [this set of EFF articles][2]
  
  <!-- .element: class="fragment" -->
  - _Transparency is a virtue of the naive_ 
	- remember, we're not doing politics
	
	<!-- .element: class="fragment" -->

  <!-- .element: class="fragment" -->
  
<!-- .element: class="fragment" -->
* In the context of todays lecture, one type of encrypted
  communication protocol will help us solve three fundamental
  questions
  - How to securely log in to a server?
  
  <!-- .element: class="fragment" -->
  - How to securely interact with a server?
  
  <!-- .element: class="fragment" -->
  - How to securely exchange any kind (and amount) of data with a server?
  
  <!-- .element: class="fragment" -->
  
<!-- .element: class="fragment" -->
		
[1]: https://en.wikipedia.org/wiki/Zombie_(computing)
[2]: https://ssd.eff.org/
