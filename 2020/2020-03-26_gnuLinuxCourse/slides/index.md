# Introduction to GNU/Linux (Shell) 

## 26 March 2020

<div style="top: 6em; left: 0%; position: absolute;">
    <img src="theme/img/lcsb_bg.png">
</div>

<div style="top: 5em; left: 60%; position: absolute;">
    <img src="slides/img/r3-training-logo.png" height="200px">
    <br><br><br>
    <h1>GNU/Linux Basics</h1>
    <br><br><br>
    <h4>
        Kondić Todor, Dr<br><br>
        todor.kondic@uni.lu<br><br>
        <i>Luxembourg Centre for Systems Biomedicine</i>
    </h4>
</div>


<style>
.multicol{
    display: flex;
}
.col{
    flex: 1;
}
</style>
