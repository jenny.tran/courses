# SSH Session From a Terminal

```shell
	# Connect to a host, on a particular port with a particular address
	# with a given username.
	# If password-based authentication is enabled, you will be presented
	# with a password prompt.

	ssh -p 8022 tk2020@10.240.16.69

	# A much more secure approach (sometimes the only option, if other
	# forms of authentication are disabled on the server) is use key based
	# authentication.

```
