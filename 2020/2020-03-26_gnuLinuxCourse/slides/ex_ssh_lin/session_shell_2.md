# SSH Session From a Terminal: Key Based Authentication

```shell
	# First, generate the key if you do not have it (if you do, it will be
	# present in ~/.ssh directory). You will be presented by a series of
	# dialogues for which defaults are ok. Please choose a strong
	# passphrase when prompted.

	ssh-keygen -b 4096

	# Next, add the key to your ssh-agent. This prevents you from having
	# to type the passphrase each time you want to log in. Accept the
	# default suggestion.

	ssh-add

	# No ssh agent present? Then run
	#
	# ssh-agent > agent.env.out
	# source agent.env.out
	#

	# Try to connect again.

	ssh -p 8022 tk2020@10.240.16.69
```
