# File Explorer: Copy(cp), Move(mv) and Remove(rm) File Operations

* Removing files and directories.

```console
$ rm linux-practical-03/pr3-hello-world.txt # Deletes the file.
$ # History, one up then adapt the command into
$ cd linux-practical-03
$ ls    # Check if it is indeed deleted.
$ rm -R dump2   # Recursively remove directory dump2 and all of its contents.
$ history 10 # Just to remind yourself what you did in last ten steps.
$ cd

```

