# Command Line


*  **Command prompt:** Non-editable; May provide contextual information
*  **Command area:** Typed commands are shown here
*  **RETURN or ENTER:** Pressing this key will execute the command

* General form of the commands
  ```shell
  prompt$ ls
  prompt$ ls -a
  prompt$ ls --help
  prompt$ rsync -avz /path/to/file/a /path/to/file/b
  prompt$ make -j8
  prompt$ gfortran -c -mtune=native
  ```
