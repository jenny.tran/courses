# File Explorer: Basics (Concepts)

* While interacting with a shell, we are always in a given *working directory*
* Data is organised into directories and files, like on other systems.
* Location of a file in respect to the current directory is given by a (*relative*) file path
  - some/dir/with/a/file

* New commands:
  - `pwd` : outputs the current working directory
  - `ls [-lhd ... file1 file2 file3 ...]` : lists files 
  - `cd path/to/existing/dir` : change current directory to a new directory
  - `mkdir [-p] path/to/new/dir` : creates a new directory
  - `touch file1 [file2 file3  ...]` : creates an empty file.
  - `history` : view the list of commands executed so far 
