# File Explorer: Copy(cp), Move(mv) and Remove(rm) File Operations

```console
$ mkdir linux-practical-03
$ cp linux-practical-01/pr1-hello-world.txt linux-practical-03/pr3-hello-world.txt #    cp arg1 arg2: copy file arg1 to file arg2
$ mkdir linux-practical-03/dump
$ cp -R linux-practical-01 linux-practical-03/dump # Copy the entire content of linux-practical-01 
$                                                  # to dump subdirectory of linux-practical-03
$ ls linux-practical-03/dump    # Check if linux-practical-01 is there.
$ # Move one up in history, jump to the beginning of the line ('Ctrl'+'A'), then
$ # replace 'ls' with 'mv'
$ # From then on ...
$ mv linux-practical-03/dump linux-practical-03/dump2 # Congratulations, you just renamed dump to dump2.
$ ls linux-practical-03    # Check the content. The directory dump should not exist.

```
