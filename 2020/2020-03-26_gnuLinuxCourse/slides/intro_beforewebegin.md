# Or, the Beginning of One?

* Learning GNU/Linux has never been easier!
  + An explosion of [resources for self-study][1]
  
  <!-- .element: class="fragment" -->
  + Of course, your [search engine][2]
  
  <!-- .element: class="fragment" -->
  + Q&A sites and on-line communities
	+ [Stack Overflow Unix Portal][3]
	+ [Reddit][4]
	+ ...
	
  <!-- .element: class="fragment" -->


<!-- .element: class="fragment" -->
  
  
[1]: https://git-r3lab.uni.lu/R3/school/courses/blob/develop/2020/2020-02-15_gnuLinuxCourse/resources.md
[2]: https://duckduckgo.com
[3]: https://unix.stackexchange.com/
[4]: https://www.reddit.com/r/linux/
