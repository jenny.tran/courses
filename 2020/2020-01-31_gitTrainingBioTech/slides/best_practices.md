# Best practices

* Fetch the upstream before creating a new branch.
* Work on your <font color="red">own</font> branch (in your own fork), and **not** on `master` and **not** on `develop`
* Get your code **reviewed** by your peers (submit a PR!)
* Submit a PR **often**!
* `Push` often - avoid conflicts

Remember: **A `push` a day keeps conflicts away!**
