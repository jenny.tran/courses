# Overview

1. Introduction 📚
    1. What is `git`? What is the use of `git`? 📚
    2. GitHub and GitLab 📚
2. Installation & set up
    - Configure git 💻
    - Configure an SSH key 💻
3. Before starting to contribute to a repository
    - What is a fork? 📚
    - Create and clone a fork 💻
4. Contributing to a repository
    - What are branches? 📚
    - Create a branch 💻
    - Create and edit a new card 💻
5. Submit the contribution for review
    - What is a merge request? 📚
    - Submit a merge request 💻
    - Review a merge request 💻
6. Summary 📚