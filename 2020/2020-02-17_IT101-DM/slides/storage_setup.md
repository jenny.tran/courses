# Storage set-up

* Download Anti-virus software
* Encrypt movable media

### Backup
  * take care of your own backups!
  * don't work on your backup copy!
  * minimum is <b>3-2-1 backup rule</b>
  
<div class="fragment" style="display:inline-grid;grid-template-columns: repeat(3, 1fr);">
<div class="content-box">
  <div class="box-title green">Personal research data</div>
  <div class="content"> 
  
  * Working documents on your laptop 
  * Online share (DropIt, OwnCloud) 
  * Copy on an external hard drive
   
  </div>
</div>

<div class="content-box">
  <div class="box-title blue">Group research data</div>
  <div class="content"> 
  
  * Research data generated in group
  * Back-up by central IT 
  * Geo-resilient copy 
  
  </div>
</div>
</div>



# Storage set-up
## Backup - Central IT/LCSB
<div style="position:relative">
<img src="slides/img/LCSB_storages_backed-up.png" height="750px">
</div>
<div style="position:absolute;left:65%;top:60%">

Server administrators take care of:
* server backups
* LCSB OwnCloud backups
* group/application server backups (not always)

</div>



# Storage set-up
## Backup - personal research data
<div style="position:relative">
<img src="slides/img/LCSB_storages_backup.png" height="750px">
</div>
<div style="position:absolute;left:55%;top:70%">

<font color="red">One version should reside on Atlas!</font>

</div>



# Server is your friend!
  * Allows a consistent backup policy for your datasets
  * Keeps number of copies to minimum
  * Specification of clear access rights
  * High accessibility
  * Data are discoverable
  * Server can't be stolen
  * ...
