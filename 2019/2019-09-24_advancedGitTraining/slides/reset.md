# Reset a branch

* Enables to reset a branch back to a previous commit
* Discards ALL commits made after the selected commit HEAD

* This happens often in **practice**:
    you pushed to a branch, then realize that you made a mistake in the commit, and want to start over.



# Example: Hard reset of a branch (1)

* Start by committing two files:
```bash
# commit first file ...
$ echo "# CV of Firstname Lastname" > myCV.md
$ git add myCV.md
$ git commit -m "add cv for Firstname Lastname"
# commit second file ...
$ echo "# Biography of Firstname Lastname" > myBio.md
$ git add myBio.md
$ git commit -m "add biography for Firstname Lastname"
$ git push origin myBranch
```

* Check the commits, copy the `SHA1` of the **second last** commit:
```bash
$ git log
```



# Example: Hard reset of a branch (2)

* Use the `reset --hard` command in order to undo the commit with `<SHA1>`:
```bash
$ git reset --hard <SHA1>
```

* Check what happened in the log
* Force push your branch (overwrite the history) with `-f`:
```bash
$ git push origin myBranch -f
```



# Notes

* Alternatively, you can also remove the last commit:
```bash
$ git reset --hard HEAD~1
```
* With a `--hard` reset, the index and the working tree are reset.

* If you omit the `--hard` flag, a mixed reset is made. This resets the index, but not the working tree
```bash
$ git reset HEAD~1
```