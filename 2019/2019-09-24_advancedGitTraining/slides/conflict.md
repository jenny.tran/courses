# Conflict resolution

* A conflict occurs when two changes change the same line in a file

* Some conflict may be resolved automatically, but major conflicts
always need to be resolved manually

* Tools exist to streamline conflict resolutions, we use `kdiff3`

* Conflicts can happen during `merge`, `cherry-pick`, and `rebase`



# Example 1: Conflict resolution when locally merging (1)

* Checkout the branch `myNewBranch` and change the file `template.md`:

```bash
$ git checkout myNewBranch
```

* Use your favorite editor and type:

```bash
# Advanced git training course

## Firstname Lastname
```

* Add and commit that change.

* Checkout the branch `myBranch` and change the file `template.md`:

```bash
# Advanced git training -- Course

## Firstname Lastname
```
* Then, save, add, and commit that change.



# Example 1: Conflict resolution when locally merging (2)

* Merge the `myNewBranch` into the `myBranch` branch:

```bash
$ git merge myNewBranch
```

* A conflict appears:
```bash
$ git merge myNewBranch
Auto-merging attendees/template.md
CONFLICT (content): Merge conflict in attendees/template.md
Automatic merge failed; fix conflicts and then commit the result.
```
* Start the merge tool:
```
$ git mergetool
```



# Example 1: Conflict resolution when locally merging (3)

* This opens kdiff3 if it was properly set up. There are 3 versions:
    - **A**: version on `myBranch` before the recent change.
    - **B**: version on `myNewBranch`
    - **C**: version on `myBranch` after the recent change

* Resolve the conflict and save. Then:
```bash
$ git merge --continue
```

* If you check the status, you will find a `.orig` file. This is a backup and contains the conflict.
```bash
$ git status
$ cat template.md.orig
```
* If you do not need anymore the backup file, you can remove it.
* You can either `rm` the `.orig` file, or you can use `git clean -fdx`. **Tip:** use `--dry-run` first to list all files that would be deleted.




# Example 2: Conflict resolution when cherry-picking (1)


If you follwed **Example 1**, reset the `myBranch` branch:
```
$ git checkout myBranch
$ git reset --hard HEAD~1
```
Get the SHA1 of the commit on the `myNewBranch` branch:
```bash
$ git show myNewBranch HEAD
```
Then, cherry-pick that commit:
```bash
$ git cherry-pick <SHA1>
```



# Example 2: Conflict resolution when cherry-picking (2)

You will get a message that there is a conflict:
```
error: could not apply e3ffc09... edit content of template
hint: after resolving the conflicts, mark the corrected paths
hint: with 'git add <paths>' or 'git rm <paths>'
hint: and commit the result with 'git commit'
```
* Start the merge tool as before:
```
$ git mergetool
```
* Resolve the conflict and save. Then:
```bash
$ git cherry-pick --continue
```
The remaining steps are the same as explained in **Example 1**.