# Overview

1. Installation and getting started

2. Amend last commit

3. Resetting to a previous commit

4. Reverting commits

5. Rebasing in Git

6. Git cherry-picking

7. Merging branches

8. Conflict Resolution
