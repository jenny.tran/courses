# Getting Started (1)

Make sure that your git is configured properly:
```bash
$ git config --global user.name "Firstname Lastname"
$ git config --global user.email "first.last@uni.lu"
```

Test whether your username and email have been registered

```bash
$ git config --list
```


Fork and then clone the tutorial repository
<a href="https://git-r3lab.uni.lu/R3/school/git/advanced-practice">https://git-r3lab.uni.lu/R3/school/git/advanced-practice</a>

```bash
$ git clone ssh://git@git-r3lab-server.uni.lu:8022/<first.last>/advanced-practice.git
```



# Getting Started (2)

* Please generate your SSH before with `$ ssh-keygen -t rsa` and set it in Gitlab!

* Add a remote `upstream`
    ```bash
    $ cd advanced-practice
    # add upstream URL
    $ git remote add upstream ssh://git@git-r3lab-server.uni.lu:8022/R3/school/git/advanced-practice.git
    $ git fetch upstream
    ```
* Check the remotes with:
    ```bash
    $ git remote -v
    ```

* Create the `develop` branch and your own branch `myBranch` based on the `develop` branch from `upstream` using the `-b` flag
    ```bash
    $ git checkout -b develop upstream/develop
    $ git checkout -b myBranch
    ```



# Install the mergetool `kdiff3`

* Download it here: http://kdiff3.sourceforge.net/

* Setting up `kdiff3`:

```bash
$ git config --global --add merge.tool kdiff3
$ git config --global --add mergetool.kdiff3.path "<kdiff3 path>"
```

* omit `""` when setting up on Linux or macOS

**Note**: On UNIX, you can find the path of `kdiff3` by typing:
```bash
$ which kdiff3
```
On Windows, the path might be `C:/Program Files/KDiff3/kdiff3.exe`.




# A note on common commands:

This workshop will not cover in detail the following commands, assuming you are familiar with them:

- `git checkout`
- `git add`
- `git commit`
- `git log`
- `git show`

Feel free to ask any questions if you run into any issues!

For your reference:
```bash
$ git <command> --help
```
Replace `<command>` with the command you want help for.

Exit with `q`
