
library(ggplot2)
library(data.table)
library(ggpubr)
data(iris)
iris <- data.table(iris)
iris <- iris[c(1:103)]

g1 <- ggplot(iris, aes(x = Species, y = Sepal.Length))+
  geom_bar(aes(fill = Species),stat="summary", fun.y="mean" ) +guides(fill = F)+
  ylim(c(0,8))

g2 <- ggplot(iris, aes(x = Species, y = Sepal.Length))+
  geom_boxplot(aes(fill = Species))+
  ylim(c(0,8))+guides(fill = F)

g3 <- ggplot(iris, aes(x = Species, y = Sepal.Length))+
  geom_boxplot(aes(fill = Species))+
  ylim(c(0,8))+ geom_point( position="jitter")+
  guides(fill = F)

ggarrange(g1, g2, g3, nrow = 1)+ggsave(filename = "../plot-data.png", device = "png", width =12, height = 6)
