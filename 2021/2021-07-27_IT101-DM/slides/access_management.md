# Access Management
<div style="display:grid;grid-gap:100px;grid-template-columns: 40% 40%">
<div >

## UNI-LU Account
  * Mails services including web mail: <a href="https://owa.uni.lu" style="color:blue; font-size:0.8em;">owa.uni.lu</a>
  * Microsoft Office & Teams: <a href="https://service.uni.lu" style="color:blue; font-size:0.8em;">service.uni.lu</a>
  * SAP services: <a href="https://fiori.uni.lu/fiori" style="color:blue; font-size:0.8em;">fiori.uni.lu/fiori</a>
  * University's Intranet: <a href="https://intranet.uni.lux" style="color:blue; font-size:0.8em;">intranet.uni.lux</a>
  * All remote work services : Webex, Slack...
  

</div>
<div>
<div class="fragment">

## LUMS Account
  * go to <a href="https://service.uni.lu" style="color:blue; font-size:0.8em;">service.uni.lu</a>: make a request and  go to <a href="https://lcsb-cdc-lums-02.uni.lu" style="color:blue; font-size:0.8em;">lums.uni.lu</a> to activate your account upon receiving credentials 

  <div style="position:absolute;left:20%;top:90%">
  <img src="slides/img/access.png" height="400px">
  </div>

  <div style="position:absolute;left:60%;top:90%">
  <img src="slides/img/lums.png" height="400px">
  </div>  
<div class="fragment">  

    * Owncloud: <a href="http://owncloud.lcsb.uni.lu" style="color:blue; font-size:0.8em;">owncloud.lcsb.uni.lu</a>
    * R3 Gitlab: <a href="https://gitlab.lcsb.uni.lu" style="color:blue; font-size:0.8em;">gitlab.lcsb.uni.lu</a>
    * Private Bin: <a href="https://privatebin.lcsb.uni.lu" style="color:blue; font-size:0.8em;">privatebin.lcsb.uni.lu</a> 
    * LCSB Wiki page, Webdav, LCSB Analytics page


</div>
</div>
