# Overview

1. What is `git`? What is the use of `git`?
2. GitHub and GitLab
3. The terminal
4. Installation of `git`
5. The editor
6. How do I configure `git`?
7. Where and how to start?
8. What is a fork?
9. What are branches?
10. The 5 essential commands (`pull` / `status` / `add` / `commit` / `push`)
11. What are merge/pull requests?
12. How do I synchronize my fork?
13. Best practices