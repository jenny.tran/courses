# Code versioning
<div style="position:absolute; width:40%">

**git**

  * Current standard for code versioning
  * Maintain versions of your code as it develops
  * Local system, which does not require an online repository
  * Repositories allow distributed development

<img  align="middle" height="300px" src="slides/img/Git-logo.png">
</div>

<div class="fragment" style="position:absolute; left:50%; width:40%"">

**git@lcsb** 
* Recommended, supported repository
* Allows tracking of issues
* Ready for continous integration - code checked on commits to the repository.
* [https://git-r3lab.uni.lu](https://git-r3lab.uni.lu)


  **Use at LCSB** 

   * All analyses code should be in a repository
      * Minimally at submission of a manuscript
      * Better daily
      * Even better "analyses chunkwise"
</div>

<div style="position:absolute; width:45%; left:50%; top:28em; text-align:right">
<a href="https://howto.lcsb.uni.lu/?policies:LCSB-POL-BIC-07" style="color:grey; font-size:0.8em;">LCSB-POL-BIC-07 Source Code Management Policy</a>
</div>
