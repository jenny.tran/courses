# Available data storage
<div style="position:absolute">
<img src="slides/img/LCSB_storages_full.png" height="750px">
</div>

<div style="position:relative">
<img src="slides/img/LCSB_storages_personal-crossed.png" height="750px">

<div style="position:absolute;left:65%;top:60%">

* Unless consortium/project has formally agreed to use a secure commercial cloud

</div>

</div>

<div style="position:absolute; width:45%; left:50%; top:28em; text-align:right">
<a href=" https://howto.lcsb.uni.lu/?policies:LCSB-POL-BIC-02" style="color:grey; font-size:0.8em;">Data Storage and Backup Policy</a>
</div>


