# What is `git`?

`git` is a **version control system** (VCS) for tracking changes in computer files and coordinating work on those files among multiple people [1].

Designed and implemented in 2005 by **Linus Torvalds**

<div align="center">
<img src="slides/img/linus.jpg">
</div>


[1] *https://en.wikipedia.org/wiki/Git*



## The inventor of `git`

<div align="center">
<img src="slides/img/git_definition.png">
</div>

`I'm an egotistical bastard, and I name all my projects after myself.
First Linux, now git.`
Linus Torvalds (2007-06-14)



# What is the use of `git`?

<div align="center">
<img src="slides/img/Git-logo.png" height="120">
</div>

* Ability to recover old versions
* No need to fully rewrite text or code; **reuse** and **save time**
* Keep the changes you made over time (**history**) to every single line
* Annotate each change with description
* **Backtrack** (if necessary) and undo unwanted changes
* Contribute content in parallel
* Easily **add contributions** of your collaborators to any version


note:

Other points to mention:

* git stems from SW development domain with >100 of people contributing to one project
* git shall not be considered as a nuisance, but as a tool that should help to track and trace the code.
* git is not to track performance. Not using it shows exactly the opposite.
