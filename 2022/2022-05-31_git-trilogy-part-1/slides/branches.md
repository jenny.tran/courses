# New term:  <font color="color:red">branch</font>

* A sequence of commits (changes) is referred to as *branch*.
* A new branch can be created by *branching off* from a commit on another branch.
* A *branch* can be understood as a named *version* of your repository.

<div style="position:absolute;left:50%;top:1em">
<img src="slides/img/branch_branch-off_detail.png" class="as-is" height="800px"/>
</div>



# Development scheme

Generally, in a repository, there are guidelines for contributing.

<br>

A common development scheme is dual with a:

- **development** version on `develop`
  - things are still being changed and mistakes are being fixed
- **stable** version on `master`
  - actively used version in production environment


<div class="fragment">
<div style="position:absolute;left:60%;top:1em">
<img src="slides/img/branch_master-develop.png" height="800px">
</div>

<font color="red">In the practice repository, the development branch is called `develop`!</font>

<div class="fragment">

![bulb](slides/img/bulb.png) Use this dual development scheme for your own repositories!



# Create your own version

Assume that you want to work on a new content.

Best practice is to <font color="red">create a new **branch**!</font>

<div class="fragment">
<div style="position:absolute;left:60%;top:1em">
<img src="slides/img/branch_my-branch.png" height="800em"/>
</div>

<br>

Using branches ensures:
* your changes are made separately from other (unrelated) changes
* your version will not be affected by other contributors
* all related changes are aggregated in one place
* work can continue if the development of one feature gets stuck

<br>
Are you working on more features in parallel?

<font color="red">Create a new **branch**!</font> for each of them!





# Quick recap

What is the difference between a *fork* and a *branch*?

<div class="fragment">
<br><br><br>
<div align="center">
<img src="slides/img/forking_detail-with-branches.png" height="500em"/>
</div>




# How to switch between branches?

<div align="center">
<img src="slides/img/branch_GUI_switch.png" height="500em"/>
</div>


![bulb](slides/img/bulb.png) Why is it best practice to branch off from `develop`?