# Synchronize your fork

As mentioned earlier, your fork has been created a certain moment in time.

How to keep it updated?

## Easy way

Delete your fork and re-create it!

- Go to `Settings > General`
- Scroll down to the `Advanced` section and expand it
- Click on `Delete this project` and follow the on-screen instructions

![bulb](slides/img/bulb.png) Before you do this, please make sure that all your merge requests have been accepted.

## Automatic way

Follow the instructions on the How-To card: [https://howto.lcsb.uni.lu/?contribute:mirror-fork](https://howto.lcsb.uni.lu/?contribute:mirror-fork)