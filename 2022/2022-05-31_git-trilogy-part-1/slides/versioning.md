# Why use a version control system?

Basic features:

* Maintain different versions of documents or code
* Ability to recover previous versions
* Track all changes to a document or code

<div style="position:absolute; left:50%">
<img src="slides/img/version-history_OneDrive.png" height="650px">
</div>

Common tools you are already using supporting "version control":

* Owncloud
* OneDrive
* ...

From the list of files, it is clear *who* changed which file and *when*.


<div class="fragment">

But some information is missing:

- *What exactly* was changed? (which section or row?)
- *Why* was it changed?



# Why use a version control system?

## Manual file versioning + CHANGELOG

<br>

<div style="position:absolute; width:30%">

Your folder:
```
my-document_v01.docx
my-document_v02.docx
...
my-document_v12.docx
CHANGELOG.txt
```
</div>
<div style="position:relative; left:40%; width:60%"">

CHANGELOG.txt:
```
v01 -> v02
- Chapter 1 updated to contain last findings
- fixing typo on page 3

v02 -> v03
- rephrasing section 2 to adhere to standard
- add missing diagram
...
```
</div>

<br>
<br>

<div class="fragment">

**What exactly** has changed? - Solved!

**Why** it was changed? - Solved!
</div>
<div class="fragment">
Really?

note: overhead for doing this manually is huge
</div>



# Using versioning system?

What happens when **3** people contribute?

```
my-document_v01_AK.docx
my-document_v01_AK_BP.docx
my-document_v02_revised.docx
my-document_v02_LP_revised-and-approved.docx
my-document_v002_draft_LP-AK_do-not-touch.docx
...
my-document_v12_we-made-it.docx
CHANGELOG.txt
```

<div class="fragment">

What happens when **10** people contribute?