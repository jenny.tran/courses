# Workflow - Web IDE

To change content of a file:

* Browse to *your fork*
* Open Web IDE
<!-- .element: class="fragment" data-fragment-index="1" -->
* Select the development branch
<!-- .element: class="fragment" data-fragment-index="2" -->
* Edit file content
<!-- .element: class="fragment" data-fragment-index="3" -->
* Commit
<!-- .element: class="fragment" data-fragment-index="4" -->
  * Click Commit
  * Select **Create a new branch**
  * Type your branch name (optional, but recommended)
  * Check **Start a new merge request**
  * Click Commit


<div style="position:absolute;left:30%;top:1em" class="fragment fade-in-then-out" data-fragment-index="1">
<img src="slides/img/commit_GUI_web-ide.png" width="95%" >
</div>

<div style="position:absolute;left:40%;top:1em" class="fragment fade-in-then-out" data-fragment-index="2">
<img src="slides/img/web-ide_select-branch.png" width="60%" >
</div>

<div style="position:absolute;left:33%;top:1em" class="fragment fade-in-then-out" data-fragment-index="3">
<img src="slides/img/web-ide_edit-file.png" width="90%" >
</div>

<div style="position:absolute;left:50%;top:1em" class="fragment fade-in-then-out" data-fragment-index="4">
<img src="slides/img/web-ide_commit-button.png" width="90%" >
</div>

<div style="position:absolute;left:50%;top:10em" class="fragment fade-in-then-out" data-fragment-index="4">
<img src="slides/img/web-ide_commit-detail.png" width="90%" >
</div>
