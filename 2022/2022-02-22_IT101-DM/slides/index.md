# IT101 - Working with computers
<br>IT101 - Working with computers<br>
## Feb 22th, 2022

<div style="top: 6em; left: 0%; position: absolute;">
    <img src="theme/img/lcsb_bg.png">
</div>

<div style="top: 5em; left: 60%; position: absolute;">
    <img src="slides/img/r3-training-logo.png" height="200px">
    <br><br><br><br>
    <h3></h3>
    <br><br><br>
    <h4>
        Nene Barry/Vilem Ded<br>
        Data Steward<br>
        nene.barry@uni.lu/lcsb-datastewards@uni.lu<br>
        <i>Luxembourg Centre for Systems Biomedicine</i>
    </h4>
</div>
