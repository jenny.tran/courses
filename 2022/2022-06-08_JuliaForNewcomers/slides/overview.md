# Overview

1. Why would you learn another programming language again?
2. Bootstrapping, working with packages, writing a script (30m)
3. Language and syntax primer (35m) with a pause (10m) in the middle
4. Getting the data in and out (15m)
5. Running programs on ULHPC (15m)
6. Questions, hands-on, time buffer (15m)
