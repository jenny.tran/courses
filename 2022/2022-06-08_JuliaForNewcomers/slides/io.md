
<div class=leader>
<i class="twa twa-bar-chart"></i>
<i class="twa twa-blue-book"></i>
<i class="twa twa-computer-disk"></i>
<i class="twa twa-chart-increasing"></i><br>
Working with data
</div>



# What do we usually need?

- we need a comfortable abstraction over "long" tabular data → *data frames*
- we need to get the data in and out → *IO functions*
- we need to make pictures → *plotting packages*



# Writing files

All at once:
```julia
write("file.txt", "What a string!\n")
write("data.bin", UInt32[1,2,3])
```

By parts (with streaming, etc.):
```julia
f = open("file.txt", "w")
println(f, "This is my string!")
# ...
close(f)
```

Better:
```julia
open("file.txt", "a") do f
    println(f, "The string again!")
    # ...
end
``` 



# Reading files

All at once:
```julia
read("file.txt", String)
open(x -> collect(readeach(x, UInt32)), "data.bin", "r")
```

Process all lines:
```julia
for line in eachline("file.txt")
    println("got a line: " * line)
end
```

Manually:
```julia
open("inputs.txt", "r") do io
    a = parse(Int, readline(io))
    b = parse(Int, readline(io))
    println("$a * $b = $(a*b)")
end
```



# Extremely useful: string interpolation

Pasting strings manually is _boring_.

```julia
"String contains $val and $otherval."

do_something("input$i.txt", "output$i.txt")

"$str <<-->> $(reverse(str))"
```

The conversion to actual strings is done using `show()`. (Customize by overloading!)



# Reading and writing structured data

```julia
using DelimitedFiles

mtx = readdlm("matrix.tsv", '\t', Int, '\n')   # returns a Matrix{Int}

writedlm("matrix.csv", ',')
```



# Data frames

Package `DataFrames.jl` provides a work-alike of the data frames from
other environments (pandas, `data.frame`, tibbles, ...)

```julia
using DataFrames

mydata = DataFrame(id = [32,10,5], text = ["foo", "bar", "baz"])

mydata.text

mydata.text[mydata.id .>= 10]
```

Main change from `Matrix`: *columns are labeled and their types differ*, also entries may be missing



# DataFrames

Popular way of importing data:
```julia
using CSV
df = CSV.read("database.csv", DataFrame)       # can also do a Matrix

CSV.write("backup.csv", df)
```

Popular among computer users:
```julia
using XLSX
x = XLSX.readxlsx("important_results.xls")

XLSX.sheetnames(x)

DataFrame(XLSX.gettable(x["Results sheet"])...)
```

<small>(Please do not export data to XLSX.)</small>



# Plotting

<center>
<img src="slides/img/unicodeplot.png" width="40%" />
</center>



# Usual plotting packages

-   `UnicodePlots.jl` (useful in terminal, https://github.com/JuliaPlots/UnicodePlots.jl)
-   `Plots.jl` (matplotlib workalike, works with Plotly)
-   `GLMakie.jl` (interactive plots)
-   `CairoMakie.jl` (PDF export of Makie plots)

Native `ggplot` and `cowplot` ports are in development.

Gallery available: https://makie.juliaplots.org
