# How do I start working on a repository?

You have to `clone` your fork first:

```bash
$ git clone ssh://git@gitlab.lcsb.uni.lu:8022/<first.last>/basic-practice-pages.git
```

If you did not configure your SSH key, clone using HTTPS:
```bash
$ git clone https://gitlab.lcsb.uni.lu/<first.last>/basic-practice-pages
```

You will be prompted to enter your credentials.

Upstream (original) repository:  https://gitlab.lcsb.uni.lu/R3/school/git/basic-practice-pages.git