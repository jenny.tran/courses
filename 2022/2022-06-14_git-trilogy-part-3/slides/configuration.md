# How to configure `git`?

```bash
$ git config --global user.name "Firstname Lastname"
$ git config --global user.email "first.last@uni.lu"
```

Test whether your username and email have been registered

```bash
$ git config --list
```

This should list the configuration with `user.name` and `user.email`.

Exit by typing `q`.
