# Overview

1. The terminal
2. Installation of `git`
3. How do I configure `git`?
4. Where and how to start?
5. The 5 essential commands (`pull` / `status` / `add` / `commit` / `push`)
6. How do I synchronize my fork?
7. What if things go wrong?
8. Best practices