# Switch between branches

List all branches of the repository with
```bash
$ git branch -a
```

Exit by typing `q`. The branch with the * is the current branch.

<div class="fragment">

Checkout another branch
```bash
$ git checkout <branchName>
```

<div class="fragment">

You can switch to the `develop` branch with
```bash
$ git checkout develop
```
If the local branch does not exist but the remote does, it is created automatically.

<div class="fragment">

<img src="slides/img/icon-live-demo.png" height="100px">



# Create your own version

Assume that you want to work on a file:

<div class="fragment">

<font color="red">Create a new **branch**!</font>

```bash
$ git checkout -b myBranch
```
The `-b` flag creates the branch. Locally, you have your own version now:
<img src="slides/img/branch-create.png" class="branch-create" height="500em"/>




Push your version to your fork:
```bash
$ git push origin myBranch
```


<div class="fragment">

<img src="slides/img/icon-live-demo.png" height="100px">
