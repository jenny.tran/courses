# Amend or revert a commit

* `git commit --amend`: change a commit on `HEAD` (the last commit)

* `git revert <SHA1>`: delete committed commits by reverting the changes.

* A trace is kept in history of the original commit and the reverted one.



# Rebasing (1)

* `git rebase` enables to shift forward your commits in time
* Move/combine a sequence of commits to a new base commit
* Avoid discrepancies when multiple people work on the same project
* Linear git history (no merge commits)
* Rebasing is like saying, “I want to base my changes on what everybody has already done.”

Imagine the following situation:
<div style="top: 14em; left: 25%; position: absolute;">
    <img src="slides/img/beforeRebase.png" height="500px">
</div>

* There are commits on `develop` that aren't in `myBranch`.



# Rebasing (2)

* After rebase, the commits in the `myBranch` branch will be place on top of `develop`.

<div style="top: 5em; left: 25%; position: absolute;">
    <img src="slides/img/afterRebase.png" height="500px">
</div>



# Cherry-picking

* Cherry-picking allows to pick one (or more) specific commits from a list of commits.

* Only the chosen commit(s) are picked, not everything up to that commit.

<div style="top: 8em; left: 25%; position: absolute;">
    <img src="slides/img/cherryPick.png" height=500px>
</div>



# Partial chery-picking

* Partial cherry-picking allows you to unpack the changes from a commit.

* Imagine you committed many files, and you want to remove certain files.

* In practice:

    - You commited all files, and you realize that there is your data inside!

    - You have committed accidentally sensitive data, such as your password

    - You committed hidden files, for instance `.DS_Store` files

    - ...




# Merging branches locally

* Merge a branch into another one locally
* Combines all the commits from a source branch onto a target branch
* In practice, this is very useful if you 'just want to try out something', or 'draft' something




# Conflict resolution

* A conflict occurs when two changes change the same line in a file

* Some conflict may be resolved automatically, but major conflicts
always need to be resolved manually

* Tools exist to streamline conflict resolutions, we use `kdiff3`

* Conflicts can happen during `merge`, `cherry-pick`, and `rebase`




# Reset a branch

* Enables to reset a branch back to a previous commit
* Discards ALL commits made after the selected commit HEAD

* This happens often in **practice**:
    you committed to a branch, then realize that you made a mistake in the commit, and want to start over.