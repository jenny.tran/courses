# Data transfer and integrity
  * When sending data: <font color="red">Do not use emails, use secure platforms (Cloud, Aspera, Atlas share...)!</font>

<div class="fragment">
Data can be corrupted:

  * (non-)malicious modification
  * faulty file transfer
  * disk corruption
</div>
  
<div class="fragment">

### Solution

 * disable write access to the source data
 * generate checksums!
 
<div style="position:absolute;left:40%;top:30%">
<img src="slides/img/checksum.png" width="500px">
</div>
</div>

<div class="fragment" style="position:relative; left:0%">


## When to generate checksums?
* before data transfer
  - new dataset from collaborator
  - upload to remote repository

* long term storage
  - master version of dataset
  - snapshot of data for publication
</div>

<div style="position:absolute; width:45%; left:50%; top:28em; text-align:right">
<a href=" https://howto.lcsb.uni.lu/?policies:LCSB-POL-BIC-02" style="color:grey; font-size:0.8em;">Data Storage and Backup Policy</a>
</div>



# Data transfer and integrity
## Encryption
<div style="position:relative;left:25%;top:60%">
<img  align="middle" height="300px" src="slides/img/encryption.png">
</div>

<div class='fragment'>

- Guaranted confidentiality
</div>

<div class='fragment'>

- Encryption key need to be kept safe
</div>
<div class='fragment'>

- <font color='red'> Loosing your encryption key means loosing your data! </font>

</div>
<div class='fragment'>

- When a Master copy of the LCSB Research Data is encrypted, the encryption key  <font color= red>must be shared with the Data Custodian</font> or authorized system administrator.

</div>



# Password exchange channels
<div style="position:relative">
<img src="slides/img/privateBin.png" height="350px">
</div>
<div style="position:absolute;left:65%;top:85%">


* Free service provided by LSCB at <a href="https://privatebin.lcsb.uni.lu" style="color:blue; font-size:0.8em;">privatebin.lcsb.uni.lu</a> 
* **LUMS** account is required
* Set expiry period
* Can expire upon first access
* Password only accessible by sender and recipient
</div>

