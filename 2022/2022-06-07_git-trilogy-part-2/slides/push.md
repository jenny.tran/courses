# Save your changes to server - push them

Your changes are saved and committed locally.

You need to save them on server. In git jargon: you need to *push them into your fork*.

2 scenarios:

- If you created branch locally, it does not exist yet on the server. You need to click on `Publish Branch` first:

<img height="230px" src="slides/img/publish.png">

For all future changes or when the branch already existed, click on `Sync changes`:

<img height="330px" src="slides/img/push.png">