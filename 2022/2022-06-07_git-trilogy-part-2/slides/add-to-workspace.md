# Add an already locally existing repository to workspace

![bulb](slides/img/bulb.png) If you already have downloaded a repository, you can add it to VS code by following:

- Go to **File > Add Folder to Workspace**

<center>
<img width="30%" src="slides/img/add-root-folder.png">
</center>