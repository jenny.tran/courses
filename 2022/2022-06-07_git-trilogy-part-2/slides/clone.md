# Clone your fork

Every repository is on the server. If you want to contribute using VS code, you need to download it to your laptop.

In git jargon: you need you *clone your fork*:
- Go to your projects (top left corner with LCSB logo)
- Find **basic-practice-pages** project under your name (your fork)
- Press *Clone* and then *Clone with SSH*. URL will be in your clipboard now.
<img width="60%" src="slides/img/clone.png">
- Switch to VS Code. Go to `View` > `Command Palette` and type "clone"




# Clone your fork

- You will be asked for the URL of the remote repository (you have it in your clipboard now)

  <img height="100px" src="slides/img/clone-SSH.png">
- and the place where to put files on your computer.

  <img height="400px" src="slides/img/clone-folder.png">
- To start working, you can now click on "Add to workspace"

  <img height="100px" src="slides/img/add-to-workspace.png">
