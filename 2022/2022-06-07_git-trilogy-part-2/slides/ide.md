# Integrated development environment (IDE)

Most of you already know how to contribute to a repository directly online via Gitlab's Web IDE ([Trilogy of git, Part I](https://courses.lcsb.uni.lu/2022-05-31_git-trilogy-part-1/)).

However, Gitlab's Web IDE is rather limited if you want to:

- work efficiently
- work with several repositories at the same time
- work offline
- make changes to several documents all at once
- use find/replace features
- ...

In this course we will learn how to contribute to a common project working on you laptop via [Visual Studio Code](https://code.visualstudio.com/) (VS Code):
- Available for macOS, Linux, and Windows
- Open source (free)
- Easy to contribute using `git`
- Powerful tool and widely used
