# Typical workflow (revisited)

- Create your own copy of the original (also called `upstream`) repository (`fork` in `git` jargon)
- Download your copy to your computer (`clone` in `git` jargon)
- Create a new branch
- Make changes
- Commit changes
- <font color="red">Push changes to server</font>
- Create merge request to upstream repository

<center>
<img width="60%" src="slides/img/fork_branch-diagram-after-commit-with-back-arrow.png">
</center>