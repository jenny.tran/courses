# Create a new branch

As part of the workflow, create a branch before starting to work.

- Click on branch symbol (bottom left)

   <img height="100px" src="slides/img/branch.png">

- Choose "+ Create New Branch from..."
- Give the name to your new branch
- Select `origin/develop`

<img src="slides/img/icon-live-demo.png" height="100px">
